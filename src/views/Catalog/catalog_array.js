export default [
  {
    title: "Физиотерапия",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: "rgba(53, 203, 0, 0.5)",
    },
    innerColor: {
      backgroundColor: "rgba(53, 203, 0, 1)",
    },
    link: "/Products",
  },
  {
    title: "Рентген",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: "rgba(0, 106, 203, 0.5)",
    },
    innerColor: {
      backgroundColor: "rgba(0, 106, 203, 1)",
    },
    link: "/Products",
  },
  {
    title: "УЗИ диагностика",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: " rgba(150, 0, 203, 0.5)",
    },
    innerColor: {
      backgroundColor: " rgba(150, 0, 203, 1)",
    },
    link: "/Products",
  },
  {
    title: "Эндо Урология",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: "rgba(255, 255, 0, 0.4)",
    },
    innerColor: {
      backgroundColor: "rgba(245, 245, 0, 1)",
    },
    link: "/Products",
  },
  {
    title: "Гинекология",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: "rgba(255, 0, 0, 0.5)",
    },
    innerColor: {
      backgroundColor: "rgba(255, 0, 0, 1)",
    },
    link: "/Products",
  },
  {
    title: "Aртроскопия",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: "rgba(222, 184, 135, 0.5)",
    },
    innerColor: {
      backgroundColor: "rgba(222, 184, 135, 1)",
    },
    link: "/Products",
  },
  {
    title: "Лаборатория",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: "rgba(255, 140, 0, 0.5)",
    },
    innerColor: {
      backgroundColor: "rgba(255, 140, 0, 1)",
    },
    link: "/Products",
  },
  {
    title: "Видеосистема",
    subtitle: "When, while lovely valley teems with vapour around meand",
    colorDiv: {
      backgroundColor: "rgba(0, 0, 0, 0.5)",
    },
    innerColor: {
      backgroundColor: "rgba(0, 0, 0, 1)",
    },
    link: "/Products",
  },
];
