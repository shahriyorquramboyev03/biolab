export default[
    {
        name: 'United Kigdom',
        img: require('@/images/flag/GB-United-Kingdom-Flag-icon 1.png'),
        address: {
            text: '4 Ratcliffe Rd, Haydon Bridge, Hexham NE47 6ET, Великобритания',
            link: 'https://goo.gl/maps/VECiuEME5C62FMaV8',
        },
        phone: {
            text: '+441434684327',
            link: 'tel:+441434684327',
        },
        email: 'coop@gmail.com',
        website: {
            text: 'coop.co.uk',
            link: 'https://www.coop.co.uk/store-finder/NE47-6ET/4-ratcliffe-road?utm_source=gmb&utm_medium=organic&utm_campaign=gmb-Haydon-Bridge-Hexham',
        },
        map: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5291.225308730223!2d-2.255082177453533!3d54.97530313010363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487db9bcac04ec73%3A0x864452054b1c60b6!2sCo-op%20Food%20-%20Haydon%20Bridge!5e0!3m2!1sru!2s!4v1647952990470!5m2!1sru!2s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>`
    },
    {
        name: 'Uzbekistan',
        img: require('@/images/flag/uzbekistan-flag-png-xl 1.png'),
        address: {
            text: '9 Bunyodkor Main Street, Chilonzor district, Tashkent, Uzbekistan',
            link: 'https://goo.gl/maps/z5Q7QZqjQ8N2',
        },
        phone: {
            text: '+998 71 755-55-55',
            link: 'tel:+998717555555',
        },
        email: 'Info@biolabgroups.com',
        website: {
            text: 'www.biolabgroups.com',
            link: 'http://www.biolabgroups.com',
        },
        map: `<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2997.916593928683!2d69.21932588556672!3d41.28891966952727!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4f6dfd457bfa7c0b!2sIT%20Labs!5e0!3m2!1sru!2s!4v1646491918188!5m2!1sru!2s" allowfullscreen="" loading="lazy"></iframe>
        `
    },
    {
        name: 'Kazakhstan',
        img: require('@/images/flag/kazakhstan-flag-png-xl 1.png'),
        address: {
            text: 'просп. Республики 3/2, Нур-Султан 010000, Казахстан',
            link: 'https://goo.gl/maps/Wpr7qwYVkiJnGFv28',
        },
        phone: {
            text: '+77172439339',
            link: 'tel:+77172439339',
        },
        email: 'Kazakhstan@gmail.com',
        website: {
            text: 'a-n.kz',
            link: 'http://a-n.kz/',
        },
        map: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4589.310943057435!2d71.43016785103919!3d51.15753138945427!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x424586cc1dcffbc1%3A0xd9c358e82023276a!2sASTANA%20NURY!5e0!3m2!1sru!2s!4v1647954394999!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>`
    },
]