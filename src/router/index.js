import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/index.vue'

const page = p => {
	return () => import(`../views/${p}`)
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/partners',
    name: 'Partners',
    component: page('Partners')
  },
  {
    path: '/catalog',
    name: 'Catalog',
    component: page('Catalog')
  },
  {
    path: '/about',
    name: 'About',
    component: page('About')
  },
  {
    path: '/blog',
    name: 'Blog',
    component: page('Blog')
  },
  {
    path: '/contacts',
    name: 'Contacts',
    component: page('Contacts')
  },
  {
    path: '/certificates',
    name: 'Certificates',
    component: page('Certificates')
  },
  {
    path: '/products',
    name: 'Products',
    component: page('Products')
  },
  {
    path: '/product-info/:postId',
    name: 'ProductInfo',
    component: page('Product-info')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
